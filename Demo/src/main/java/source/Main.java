package source;

public class Main {

    public static void main(String[] args) {
        try {
            if(args.length > 1) {
                throw new Exception("More than 1 parameter");
            }

            int i = Integer.parseInt(args[0]);

            actuallyDoStuff(i);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void doStuff1(int i) {
        actuallyDoStuff(i);
    }

    public static void doStuff2(int i) {
        System.out.println(i);
    }

    public static void actuallyDoStuff(int i) {
        if(i == 50) {
            System.out.println("Error");
        }
        else {
            System.out.println("OK");
        }
    }
}
