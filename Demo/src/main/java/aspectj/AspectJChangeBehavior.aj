package aspectj;

import source.Main;


public aspect AspectJChangeBehavior {

    pointcut callActuallyDoStuff(int number, Main main):
            call(void Main.actuallyDoStuff(int)) &&
                    args(number) &&
                    target(main);

    before(int number, Main main): actuallyDoStuff(number, main) {
    }

    void around(int number, Main main):
            callActuallyDoStuff(number, main) {
        // remove callee here
    }

    after(int number, Main main): actuallyDoStuff(number, main) {
        System.out.println("Call actuallyDoStuff");
    }

}
